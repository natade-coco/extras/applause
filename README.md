# Applause Sound Server

音声再生用のAPIサーバー

- WAVファイルの登録および再生に対応しています。
- Golang製。音声関連の処理には[faiface/beep](https://github.com/faiface/beep/)を使用しています。

## Endpoint

`http://applause.default.svc.cluster.local:8080`

## API

### `POST /sounds`

音声登録API。再生する音声を登録します。

#### Headers

| key | type | optional | 説明 |
| :-- | :-- | :-- | :-- |
| `Content-Type` | multipart/form-data | | |

#### Request Body

| key | type | optional | 説明 |
| :-- | :-- | :-- | :-- |
| `id` | string | | 音声ID |
| `file` | file | | WAVファイル |
| `volume` | float | ○ | 基準ボリュームに対する増減値 |

- WAVファイルの推奨フォーマット: 16bit/22050Hz Mono (数秒以内)
- すべてのファイルについてフォーマットを統一すること

#### Response Body

| key | type | optional | 説明 |
| :-- | :-- | :-- | :-- |
| `id` | string | | 音声ID |

### `POST /sounds/:id/play`

再生要求API。指定された音声IDに対応する音声を再生します。

#### Path Params

| key | type | optional | 説明 |
| :-- | :-- | :-- | :-- |
| id | string | | 音声ID |

#### Headers

| key | type | optional | 説明 |
| :-- | :-- | :-- | :-- |
| `Content-Type` | application/json | | |

#### Request Body

| key | type | optional | 説明 |
| :-- | :-- | :-- | :-- |
| `stretch` | float | ○ | 音声の伸長率 |
| `pan` | float | ○ | Pan (-1 〜 1) |

## Commands

実行時引数

| arg | 説明 | default |
| :-- | :-- | :-- |
| `debug` | デバッグログ出力有無 | false |
| `config` | Configファイルのパス | conf/conf.json |

## Config

Configファイル(JSON)を起動時にロードします。

```json
{
  "quality": 2,       // リサンプリング時のクオリティ (1:低 〜 6:高)
  "speaker-sr": 100,  // スピーカーのサンプルレート
  "vol": -1.75,       // 音声の基準ボリューム (元の2^v倍)
}
```

## Development

```sh
cd applause
go run . --debug=true
```

## Deployment

```sh
VERSION=X.Y.Z make push
git push
```
