package conf

import (
	"encoding/json"
	"io/ioutil"
)

type Conf struct {
	Quality   int     `json:"quality"`
	SpeakerSR float64 `json:"speaker-sr"`
	Volume    float64 `json:"vol"`
}

type ConfOpts struct {
	FilePath string
}

func ReadConf(opts *ConfOpts) (*Conf, error) {
	conf := &Conf{}

	raw, err := ioutil.ReadFile(opts.FilePath)
	if err != nil {
		return conf, err
	}

	err = json.Unmarshal([]byte(raw), conf)
	if err != nil {
		return conf, err
	}

	return conf, nil
}
