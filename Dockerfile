FROM golang:1.15-alpine3.13 AS builder

RUN apk --no-cache --update add git alpine-sdk alsa-lib-dev

WORKDIR /workdir

ENV GO111MODULE="on"
COPY go.mod go.sum ./
RUN go mod download

COPY . ./
RUN CGO_ENABLED=1 GOOS=linux GOARCH=amd64 go install -ldflags "-s -w" ./...

FROM alpine:3.13

RUN apk --no-cache --update add wget ca-certificates curl alsa-lib-dev alsa-utils

WORKDIR /root/

COPY --from=builder /go/bin /root
COPY conf/conf.json /root/conf.json
COPY asound.conf /etc/asound.conf

ENTRYPOINT ["./applause"]
