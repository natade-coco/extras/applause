/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"gitlab.com/natade-coco/extras/applause/conf"
	"gitlab.com/natade-coco/extras/applause/pkg/server"
	"gitlab.com/natade-coco/extras/applause/pkg/service"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/rancher/wrangler/pkg/signals"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "applause",
	Short: "applause",
	Long:  ``,
	Run: func(cmd *cobra.Command, args []string) {
		isDebug, _ := cmd.Flags().GetBool("debug")
		setupLogger(isDebug)

		ctx := signals.SetupSignalHandler(context.Background())

		cf, _ := cmd.Flags().GetString("config")
		confOpts := &conf.ConfOpts{FilePath: cf}
		c, err := conf.ReadConf(confOpts)
		if err != nil {
			log.Fatal(err)
		}

		serverOpts := &server.ApplauseHttpServerOpts{}
		if s, err := json.Marshal(serverOpts); err == nil {
			log.Infof("ServerOpts: %s", s)
		}
		server := server.NewApplauseHttpServer(serverOpts)

		playerOpts := &service.PlayerOpts{
			Conf: c,
		}
		if s, err := json.Marshal(playerOpts); err == nil {
			log.Infof("PlayerOpts: %s", s)
		}
		player := service.NewPlayer(playerOpts)

		service := service.NewApp(server, player)
		if err := service.Start(ctx); err != nil {
			log.Fatal(err)
		}
	},
}

func setupLogger(isDebug bool) {
	log.SetFormatter(&log.JSONFormatter{
		TimestampFormat: time.RFC3339Nano,
	})

	if isDebug {
		log.SetLevel(log.DebugLevel)
	}
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	// Here you will define your flags and configuration settings.
	// Cobra supports persistent flags, which, if defined here,
	// will be global for your application.

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.applause.yaml)")

	// Cobra also supports local flags, which will only run
	// when this action is called directly.
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	rootCmd.Flags().Bool("debug", false, "Enable debug log")
	rootCmd.Flags().String("config", "conf/conf.json", "Path of applause config file")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name ".applause" (without extension).
		viper.AddConfigPath(home)
		viper.SetConfigName(".applause")
	}

	viper.AutomaticEnv() // read in environment variables that match

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err == nil {
		fmt.Println("Using config file:", viper.ConfigFileUsed())
	}
}
