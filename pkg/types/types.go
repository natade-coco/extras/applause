package types

import (
	"context"
	"io"
)

type ApplauseServer interface {
	Start(ctx context.Context) error
	Stop() error
	SetDelegate(delegate ApplauseServerDelegate)
}

type ApplauseServerDelegate interface {
	PostSound(ctx context.Context, opts SoundCreateOpts) (Sound, error)
	PostPlay(ctx context.Context, opts PlayCreateOpts) (Play, error)
}

type SoundCreateOpts struct {
	Id     string
	Reader io.Reader
	Volume float64
}

type PlayCreateOpts struct {
	Id      string
	Stretch float64
	Pan     float64
}

type Sound struct {
	Id string `json:"id"`
}

type Play struct {
	Id string `json:"id"`
}
