package server

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/natade-coco/extras/applause/pkg/types"
	"gopkg.in/go-playground/validator.v9"
)

// Request

func (s *ApplauseHttpServer) buildSoundCreateOpts(r *http.Request) (types.SoundCreateOpts, error) {
	var opts types.SoundCreateOpts

	id := r.FormValue("id")
	opts.Id = id

	file, _, err := r.FormFile("file")
	if err != nil {
		log.Error(err)
		return opts, &BadRequest{Err: Err{Message: err.Error()}}
	}
	defer file.Close()
	opts.Reader = file

	v := r.FormValue("volume")
	if v == "" {
		opts.Volume = 0.0
	} else {
		opts.Volume, err = strconv.ParseFloat(v, 64)
		if err != nil {
			log.Error(err)
			return opts, &BadRequest{Err: Err{Message: err.Error()}}
		}
	}

	return opts, validator.New().Struct(opts)
}

func (s *ApplauseHttpServer) buildPlayCreateOpts(r *http.Request) (types.PlayCreateOpts, error) {
	var opts types.PlayCreateOpts

	vars := mux.Vars(r)
	opts.Id = vars["id"]

	body := struct {
		Stretch float64 `json:"stretch"`
		Pan     float64 `json:"pan"`
	}{
		Stretch: 1.0,
		Pan:     0.0,
	}
	if err := json.NewDecoder(r.Body).Decode(&body); err != nil {
		log.Error(err)
		return opts, &BadRequest{Err: Err{Message: err.Error()}}
	}
	opts.Stretch = body.Stretch
	opts.Pan = body.Pan

	return opts, validator.New().Struct(opts)
}

// Response

func (s *ApplauseHttpServer) writeError(w http.ResponseWriter, r *http.Request, err error) {
	sc := ResponseError(w, err)
	log.WithFields(FormatRequest(r, sc, err)).Error("API Reqeust")
}

func (s *ApplauseHttpServer) writeRaw(w http.ResponseWriter, r *http.Request, i interface{}) {
	sc := ResponseRaw(w, i)
	log.WithFields(FormatRequest(r, sc, nil)).Info("API Reqeust")
}

func FormatRequest(r *http.Request, code int, err error) log.Fields {
	hdr := make(map[string]string)
	for name, headers := range r.Header {
		name = strings.ToLower(name)
		for _, h := range headers {
			hdr[fmt.Sprintf("%v", name)] = fmt.Sprintf("%v", h)
		}
	}

	fields := log.Fields{
		"context":           fmt.Sprintf("%s %d %s %s", r.Proto, code, r.Method, r.URL),
		"error":             err,
		"x-accept-language": hdr["accept-language"],
		"x-content-type":    hdr["content-type"],
		"x-referer":         hdr["referer"],
		"x-forwarded-for":   hdr["x-forwarded-for"],
		"x-user-agent":      hdr["user-agent"],
	}
	return fields
}

func ResponseRaw(w http.ResponseWriter, i interface{}) int {
	raw, err := json.Marshal(i)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return http.StatusInternalServerError
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(raw)
	return http.StatusOK
}

func ResponseError(w http.ResponseWriter, err error) int {
	errors := make([]error, 0)
	errors = append(errors, err)
	var statusCode int
	switch err.(type) {
	case *InternaleServerError:
		statusCode = http.StatusInternalServerError
	case *BadRequest:
		statusCode = http.StatusBadRequest
	case *Forbidden:
		statusCode = http.StatusForbidden
	case *NotFound:
		statusCode = http.StatusNotFound
	case *PaymentError:
		statusCode = http.StatusBadRequest
	default:
		statusCode = http.StatusInternalServerError
	}
	body := DocumentRoot{Errors: errors}
	raw, err := json.Marshal(body)
	if err != nil {
		statusCode = http.StatusInternalServerError
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(raw)
	return statusCode
}

type DocumentRoot struct {
	Data   interface{} `json:"data,omitempty"`
	Errors []error     `json:"errors,omitempty"`
}

type Err struct {
	Message string `json:"message"`
}

type InternaleServerError struct{ Err }
type BadRequest struct{ Err }
type Forbidden struct{ Err }
type NotFound struct{ Err }
type PaymentError struct{ Err }

func (s *InternaleServerError) Error() string { return s.Message }
func (s *BadRequest) Error() string           { return s.Message }
func (s *Forbidden) Error() string            { return s.Message }
func (s *NotFound) Error() string             { return s.Message }
func (s *PaymentError) Error() string         { return s.Message }
