package server

import (
	"context"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	log "github.com/sirupsen/logrus"
	"gitlab.com/natade-coco/extras/applause/pkg/types"
)

type ApplauseHttpServer struct {
	delegate types.ApplauseServerDelegate
	srv      *http.Server
	opts     *ApplauseHttpServerOpts
}

type ApplauseHttpServerOpts struct {
}

func NewApplauseHttpServer(opts *ApplauseHttpServerOpts) types.ApplauseServer {
	s := &ApplauseHttpServer{
		srv: &http.Server{
			Addr: "0.0.0.0:8080",
		},
		opts: opts,
	}
	return s
}

func (s *ApplauseHttpServer) cors() []handlers.CORSOption {
	opts := make([]handlers.CORSOption, 0)
	credentialOk := handlers.AllowCredentials()
	headersOk := handlers.AllowedHeaders([]string{"Origin", "X-Requested-With", "Content-Type", "Accept", "Authorization", "Cache-Control", "pragma"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{"GET", "HEAD", "POST", "PUT", "OPTIONS", "DELETE"})
	headersExpose := handlers.ExposedHeaders([]string{"Location"})
	opts = append(opts, headersOk, originsOk, methodsOk, credentialOk, headersExpose)
	return opts
}

func (s *ApplauseHttpServer) Start(ctx context.Context) error {
	log.Info("ApplauseHttpServer.Start")
	r := mux.NewRouter()
	r.HandleFunc("/sounds", s.PostSoundHandler).Methods("POST")
	r.HandleFunc("/sounds/{id}/play", s.PostPlayHandler).Methods("POST")

	h := handlers.LoggingHandler(os.Stdout, handlers.CORS(s.cors()...)(r))
	s.srv.Handler = h

	err := s.srv.ListenAndServe()
	return err
}

func (s *ApplauseHttpServer) Stop() error {
	log.Info("ApplauseHttpServer.Stop")
	ctx := context.Background()
	return s.srv.Shutdown(ctx)
}

func (s *ApplauseHttpServer) SetDelegate(delegate types.ApplauseServerDelegate) {
	s.delegate = delegate
}

func (s *ApplauseHttpServer) PostSoundHandler(w http.ResponseWriter, r *http.Request) {
	opts, err := s.buildSoundCreateOpts(r)
	if err != nil {
		s.writeError(w, r, err)
		return
	}
	res, err := s.delegate.PostSound(r.Context(), opts)
	if err != nil {
		s.writeError(w, r, err)
		return
	}
	s.writeRaw(w, r, res)
}

func (s *ApplauseHttpServer) PostPlayHandler(w http.ResponseWriter, r *http.Request) {
	opts, err := s.buildPlayCreateOpts(r)
	if err != nil {
		s.writeError(w, r, err)
		return
	}
	res, err := s.delegate.PostPlay(r.Context(), opts)
	if err != nil {
		s.writeError(w, r, err)
		return
	}
	s.writeRaw(w, r, res)
}
