package service

import (
	"context"

	log "github.com/sirupsen/logrus"
	"gitlab.com/natade-coco/extras/applause/pkg/types"
)

type App struct {
	server types.ApplauseServer
	player *Player
}

func NewApp(server types.ApplauseServer, player *Player) *App {
	s := &App{
		server: server,
		player: player,
	}
	server.SetDelegate(s)
	return s
}

func (s *App) Start(ctx context.Context) error {
	log.Info("App.Start")
	errCh := make(chan error, 1)
	defer close(errCh)

	go func() {
		child, cancel := context.WithCancel(ctx)
		defer cancel()
		if err := s.server.Start(child); err != nil {
			log.Fatal(err)
		}
	}()

	select {
	case <-ctx.Done():
		return s.Stop(ctx)
	}
}

func (s *App) Stop(ctx context.Context) error {
	log.Info("App.Stop")
	s.server.Stop()
	return ctx.Err()
}
