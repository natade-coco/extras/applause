package service

import (
	"fmt"
	"io"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/speaker"
	"github.com/faiface/beep/wav"
	log "github.com/sirupsen/logrus"
	"gitlab.com/natade-coco/extras/applause/conf"
	"gitlab.com/natade-coco/extras/applause/pkg/server"
	"gitlab.com/natade-coco/extras/applause/pkg/types"
)

type Sound struct {
	id     string
	buffer *beep.Buffer
	volume float64
}

type Player struct {
	sounds map[string]*Sound
	format *beep.Format
	opts   *PlayerOpts
}

type PlayerOpts struct {
	Conf *conf.Conf
}

func NewPlayer(opts *PlayerOpts) *Player {
	p := &Player{
		sounds: map[string]*Sound{},
		format: nil,
		opts:   opts,
	}
	return p
}

func (p *Player) RegisterSound(id string, r io.Reader, volume float64) (types.Sound, error) {
	var res types.Sound

	streamer, format, err := wav.Decode(r)
	if err != nil {
		return res, err
	}

	buffer := beep.NewBuffer(format)
	buffer.Append(streamer)
	streamer.Close()

	vol := p.opts.Conf.Volume + volume

	p.sounds[id] = &Sound{
		id:     id,
		buffer: buffer,
		volume: vol,
	}

	if p.format == nil {
		speaker.Init(format.SampleRate, format.SampleRate.N(time.Second/time.Duration(p.opts.Conf.SpeakerSR)))
		p.format = &format
	}

	log.Infof("Sound registered: [%s]", id)
	res.Id = id
	return res, nil
}

func (p *Player) Play(id string, stretch float64, pan float64) (types.Play, error) {
	var res types.Play

	_, ok := p.sounds[id]
	if !ok {
		msg := fmt.Sprintf("no sound with id %s", id)
		return res, &server.BadRequest{Err: server.Err{Message: msg}}
	}

	go func() {
		s := p.makeStream(id, stretch, pan)
		log.WithFields(log.Fields{"id": id}).Info("Play")
		speaker.Play(s)
	}()

	res.Id = id
	return res, nil
}

func (p *Player) makeStream(id string, stretch float64, pan float64) beep.Streamer {
	sound := p.sounds[id]
	sr := float64(p.format.SampleRate) * stretch
	log.WithFields(log.Fields{"sr": sr, "stretch": stretch, "pan": pan}).Info("makeStream")

	b := sound.buffer
	s := b.Streamer(0, b.Len())
	effected := &effects.Pan{
		Streamer: &effects.Volume{
			Streamer: s,
			Base:     2,
			Volume:   sound.volume,
			Silent:   false,
		},
		Pan: pan, // -1 〜 1
	}

	resampled := beep.Resample(p.opts.Conf.Quality, p.format.SampleRate, beep.SampleRate(sr), effected)
	return resampled
}
