package service

import (
	"context"

	"gitlab.com/natade-coco/extras/applause/pkg/types"
)

func (s *App) PostSound(ctx context.Context, opts types.SoundCreateOpts) (types.Sound, error) {
	return s.player.RegisterSound(opts.Id, opts.Reader, opts.Volume)
}

func (s *App) PostPlay(ctx context.Context, opts types.PlayCreateOpts) (types.Play, error) {
	return s.player.Play(opts.Id, opts.Stretch, opts.Pan)
}
