
IMG_NAME?=registry.gitlab.com/natade-coco/extras/applause
IMAGE_TAG?=$(shell git rev-parse --short HEAD)
VERSION?=$(IMAGE_TAG)

all: build

bump-version:
	sed -i '' "s|$(IMG_NAME):.*|$(IMG_NAME):$(VERSION)|g" $(PWD)/manifests/daemonset.yml

build: bump-version
	DOCKER_BUILDKIT=1 docker build --platform linux/amd64 -t $(IMG_NAME):$(VERSION) -f $(PWD)/Dockerfile .

push: build
	docker push $(IMG_NAME):$(VERSION)
	git add $(PWD)/manifests
	git commit -m "bump version $(VERSION)"
